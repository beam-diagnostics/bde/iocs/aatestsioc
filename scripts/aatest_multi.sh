#!/bin/bash
#
# This script will start the acquisition on all the IOCs whose prefixes
# were specified on command line.
#
# Author: Hinko Kocevar
# Created: 17 Mar 2020

set -e

export PATH=/data/bdee/aa-stage/bin:$PATH

[[ $# -gt 0 ]] || { echo "usage: $(basename $0) ioc_prefix [ioc_prefix ..]"; exit 1; }
IOC_PREFIXES="$@"

echo -n "Enable INT waveforms [n/y]? "
read -a INT

echo -n "Enable FLOAT waveforms [n/y]? "
read -a FLOAT

echo -n "Update time [seconds]? "
read -a UPDATE

echo -n "Number of iterations? "
read -a LIMIT

echo
echo current configuration:
if [[ $INT = y ]]; then
  echo INT waveforms enabled
  INT=1
else
  echo INT waveforms disabled
  INT=0
fi
if [[ $FLOAT = y ]]; then
  echo FLOAT waveforms enabled
  FLOAT=1
else
  echo FLOAT waveforms disabled
  FLOAT=0
fi
if [[ $UPDATE = "" ]]; then
  UPDATE=1.0
fi
echo update time $UPDATE s
if [[ $LIMIT = "" ]]; then
  LIMIT=-1
fi
echo number of iterations $LIMIT
echo

echo -n Continue [n/y]?
read -a ANS

if [[ $ANS != 'y' ]]; then
  echo aborting..
  exit 1
fi

# stop
for IOC in $IOC_PREFIXES; do
  echo stopping IOC LAB:$IOC:
  caput LAB:$IOC:Run 0
done
# config
for IOC in $IOC_PREFIXES; do
  echo configuring IOC LAB:$IOC:
  caput LAB:$IOC:Counter 0
  caput LAB:$IOC:UpdateTime $UPDATE
  caput LAB:$IOC:Limit $LIMIT
  caput LAB:$IOC:EnableAllInt32 $INT
  caput LAB:$IOC:EnableAllFloat64 $FLOAT
  caput LAB:$IOC:Run 1
done
# run
for IOC in $IOC_PREFIXES; do
  echo starting IOC LAB:$IOC:
  caput LAB:$IOC:Run 1
done

echo waiting 2 seconds, then checking..
sleep 2

# check
for IOC in $IOC_PREFIXES; do
  echo checking IOC LAB:$IOC:
  caget LAB:$IOC:CounterR
  caget LAB:$IOC:RateR
done
