#!/bin/bash
#
# This script will create a substitution file with desired amount of
# waveforms (if not present), create the ioc folder and files
# (if not present), define the startup macros and call start_ioc.sh to
# start the IOC.
#
# Author: Hinko Kocevar
# Created: 4 Feb 2020

set -e

[[ $# -gt 4 ]] || { echo "usage: $(basename $0) ioc_prefix num_waveforms num_samples ps_int_id ps_float_id"; exit 1; }
IOC_PREFIX="$1"
NUM_WAVEFORMS="$2"
NUM_SAMPLES="$3"
PS_INT_ID="$4"
PS_FLOAT_ID="$5"
INSTANCE=${PS_INT_ID}_${PS_FLOAT_ID}

echo "requesting IOC_PREFIX=$IOC_PREFIX"
echo "requesting NUM_WAVEFORMS=$NUM_WAVEFORMS"
echo "requesting NUM_SAMPLES=$NUM_SAMPLES"
echo "using PS_INT_ID=$PS_INT_ID"
echo "using PS_FLOAT_ID=$PS_FLOAT_ID"
echo "setting INSTANCE=$INSTANCE"

if [[ ! -f start_ioc.sh ]]; then
  echo 'please run this script from IOC ./stage folder, aborting..'
  exit 1
fi

# check for substitution file
if [[ ! -f db/aatest-$INSTANCE.substitutions ]]; then
  # create a substitution file
  echo "creating db/aatest-$INSTANCE.substitutions file .."
  cat << EOF > /tmp/aatest-$INSTANCE.substitutions
file aatests-main.template {
pattern { ADDR }
{ 0 }
}

file aatests-waveform.template {
pattern { ADDR }
EOF

  n=$((NUM_WAVEFORMS-1))
  for i in $(seq 0 $n)
  do
    echo "{ $i }" >> /tmp/aatest-$INSTANCE.substitutions
  done
  echo "}" >> /tmp/aatest-$INSTANCE.substitutions

  mv /tmp/aatest-$INSTANCE.substitutions db/aatest-$INSTANCE.substitutions
fi

# prepare the IOC folder if required
if [[ ! -d ioc/$IOC_PREFIX~$INSTANCE ]]; then
  echo "creating ioc/$IOC_PREFIX~$INSTANCE folder .."

  mkdir -p /tmp/ioc/$IOC_PREFIX~$INSTANCE
  cp ioc/st.cmd.in /tmp/ioc/$IOC_PREFIX~$INSTANCE/st.cmd
  cat << EOF > /tmp/ioc/$IOC_PREFIX~$INSTANCE/instance.cmd
###############################################################################
#- Macro line should start with '### MACRO' folowed by macro name
#- and default value:
#-   ### MACRO <MACRO NAME> <MACRO DEFAULT VALUE>
#-
#- All the lines (except ### MACRO lines) will be copied verbatim to final
#- st.cmd file in the instance folder.
#-
#- Following macros need to be defined here as they are expected in the
#- common.cmd.
epicsEnvSet("LOCATION", "LAB")
epicsEnvSet("DEVICE_NAME", "$IOC_PREFIX")
epicsEnvSet("NUM_WAVEFORMS", "$NUM_WAVEFORMS")
epicsEnvSet("NUM_SAMPLES", "$NUM_SAMPLES")
epicsEnvSet("PS_INT_ID", "$PS_INT_ID")
epicsEnvSet("PS_FLOAT_ID", "$PS_FLOAT_ID")
epicsEnvSet("INSTANCE", "$INSTANCE")
###############################################################################
EOF
  mv /tmp/ioc/$IOC_PREFIX~$INSTANCE ioc/$IOC_PREFIX~$INSTANCE
fi

# start the IOC now
./start_ioc.sh dev . $IOC_PREFIX~$INSTANCE 2000
