# aatestsioc

Author: Hinko Kocevar

Updated: 18 Mar 2020

IOC for testing Archiver Appliance.

## Features

* Float64 and Int32 waveforms are served at the same time
* control over all Float64 and Int32 waveforms
* control / monitoring of time passed between waveform updates
* control / monitoring of waveform update counter
* control / monitoring of generated waveforms limit

## Defaults

By default the IOC prefix will be `LAB:AA1:`.
Waveforms are designated by adding `WFn` to the IOC prefix.
Waveforms are not enabled by default.
Time updated between waveforms is 1.0 seconds.
Waveform counter is set to 0 at IOC startup.

## Starting the IOC

A script `aatest_start.sh` should be used to start the IOC with the desired configuration (details follow).
`/data/bdee/aa-stage/` is a top level folder from where the scripts need to be executed.

Script help:

```
	$ cd /data/bdee/aa-stage/
	$ ./bin/aatest_start.sh
	usage: aatest_start.sh ioc_prefix num_waveforms num_samples ps_int_id ps_float_id
```

Above, `ioc_prefix` specifies part of the PV name, `num_waveforms` specifies number of waveforms to generate, `num_samples` specifies number of elements in each waveform,
`ps_int_id` refers to test scenario assigned to Int32 data type and `ps_float_id` refers to test scenario assigned to Float64 data type.
A term 'single waveform' refers to two waveforms, one for Float64 and another for Int32 data type.
A certain combination of the supplied parameters defines an IOC configuration.

Here are some examples of the waveform / sample used with `aatest_start.sh`:

1 waveform, 1 000 000 elements:
```
	$ cd /data/bdee/aa-stage/
	$ ./bin/aatest_start.sh AA1 1 1000000 AAPS-0010 AAPS-0020
```

100 waveforms, 1 000 elements each:
```
	$ cd /data/bdee/aa-stage/
	$ ./bin/aatest_start.sh AA1 100 1000 AAPS-0030 AAPS-0040
```

500 waveforms, 10 000 elements each:
```
	$ cd /data/bdee/aa-stage/
	$ ./bin/aatest_start.sh AA1 500 10000 AAPS-0050 AAPS-0060
```

10 000 waveforms, 10 000 elements each, split among 8 IOCs with prefixes AA1..8:
```
	$ cd /data/bdee/aa-stage/
	$ ./bin/aatest_start.sh AA1 1250 10000 AAPS-0070 AAPS-0080
	$ ./bin/aatest_start.sh AA2 1250 10000 AAPS-0070 AAPS-0080
	$ ./bin/aatest_start.sh AA3 1250 10000 AAPS-0070 AAPS-0080
	$ ./bin/aatest_start.sh AA4 1250 10000 AAPS-0070 AAPS-0080
	$ ./bin/aatest_start.sh AA5 1250 10000 AAPS-0070 AAPS-0080
	$ ./bin/aatest_start.sh AA6 1250 10000 AAPS-0070 AAPS-0080
	$ ./bin/aatest_start.sh AA7 1250 10000 AAPS-0070 AAPS-0080
	$ ./bin/aatest_start.sh AA8 1250 10000 AAPS-0070 AAPS-0080
```

If the IOC configuration does not exist the folder with name `ioc/ioc_prefix~ps_int_id`\_`ps_float_id` will be created,
and also reused in subsequent IOC startups. This folder can be removed by user at any time, effectively removing IOC configuration.

## Controling multiple IOCs

In order to efficiently control multiple IOCs that could be running in parallel (with 
different IOC prefix in the name) one can use the `aatest_multi.sh` script.

Script takes a list of IOC prefixes and will ask which waveforms should
be enabled (integer and/or float), the time of update and number of iterations.
The IOCs need to be started ahead of time using the `aatest_start.sh` script as 
outlined above.

Usage:
```
	cd /data/bdee/aa-stage/
	$ ./bin/aatest_multi.sh 
	usage: aatest_multi.sh ioc_prefix [ioc_prefix ..]
```

Example of configuring and starting the IOC with prefix AA1 is seen below:
```
	cd /data/bdee/aa-stage/
	./bin/aatest_multi.sh AA1
	Enable INT waveforms [n/y]? y
	Enable FLOAT waveforms [n/y]? y
	Update time [seconds]? 
	Number of iterations? 

	current configuration:
	INT waveforms enabled
	FLOAT waveforms enabled
	update time 1.0 s
	number of iterations -1

	Continue [n/y]?y
	stopping IOC LAB:AA1:
	Old : LAB:AA1:Run                    Run
	New : LAB:AA1:Run                    Stop
	configuring IOC LAB:AA1:
	Old : LAB:AA1:Counter                0
	New : LAB:AA1:Counter                0
	Old : LAB:AA1:UpdateTime             1
	New : LAB:AA1:UpdateTime             1
	Old : LAB:AA1:Limit                  -1
	New : LAB:AA1:Limit                  -1
	Old : LAB:AA1:EnableAllInt32         Off
	New : LAB:AA1:EnableAllInt32         On
	Old : LAB:AA1:EnableAllFloat64       On
	New : LAB:AA1:EnableAllFloat64       On
	Old : LAB:AA1:Run                    Stop
	New : LAB:AA1:Run                    Run
	starting IOC LAB:AA1:
	Old : LAB:AA1:Run                    Run
	New : LAB:AA1:Run                    Run
	checking IOC LAB:AA1:
	LAB:AA1:CounterR               4
	LAB:AA1:RateR                  1
```

Example of configuring and starting the IOC with prefixes AA1, AA2 and AA3 is seen below:
```
	cd /data/bdee/aa-stage/
	./bin/aatest_multi.sh AA1 AA2 AA3
	...
```

## PV list

In the table below macros are substituted as follows: `P=LAB:`, `R=AAi:`.
In addition to `P` and `R`, each Int32 waveform data PV has to define the `PS_INT_ID` macro,
and Float64 waveform data PV has to define the `PS_FLOAT_ID` macro.
Waveform PV names also contain `WFnnn-`; where `nnn` runs from 0 to `num_waveforms`.

|PV name|Description|Notes|
|--|--|--|
|$(P)$(R)Run|Start the waveform updates|Increments first element of each array (array[0]=$(P)$(R)Counter)|
|$(P)$(R)RunR|Status of waveform updates, read-back value|
|$(P)$(R)NumWaveformsR|Number of waveforms being updated|Reflects `num_waveforms` from IOC startup parameter|
|$(P)$(R)NumPointsR|Number of points in a waveform|Reflects `num_samples` from IOC startup parameter|
|$(P)$(R)UpdateTime|Waveform update time in seconds|Anything >= 0.02 seconds|
|$(P)$(R)UpdateTimeR|Read-back of above|
|$(P)$(R)Counter|Waveform update counter|Can be reset to 0 or set to any other value|
|$(P)$(R)CounterR|Read-back of above|
|$(P)$(R)RateR|Achieved waveform update rate in Hz|
|$(P)$(R)EnableAllFloat64|Enable / disable processing of the Float64 waveforms|
|$(P)$(R)EnableAllInt32|Enable / disable processing of the Int32 waveforms|
|$(P)$(R)Int32ScenarioR|Int32 scenario indicator|
|$(P)$(R)Float64ScenarioR|Float64 scenario indicator|
|$(P)$(R)Limit|Number of waveforms to generate|Can be set to -1 to ignore the limit (infite waveforms)|
|$(P)$(R)LimitR|Read-back of above|
|||
|$(P)$(R)$(PS_FLOAT_ID)-WFnnn-DataFloat64|A waveform Float64 data|
|$(P)$(R)$(PS_INT_ID)-WFnnn-DataInt32|A waveform Int32 data|

## OPI

OPI for controlling the IOC is shown below.

![alt text](opi1.png "OPI display")
